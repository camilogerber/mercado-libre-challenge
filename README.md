# Mercado Libre Challenge

This challenge consists of building an application that searches for emails from Gmail that contain the word 'DevOps' in the subject or in the body and persists them in a database.

## Requeriments

* Java 8.
* Make.
* HTTP client (Postman).
* Gmail account (for testing purposes).


## Installation and usage

>To test this program, first you should send emails to your own Gmail manually with or without the word 'DevOps' in different places to generate content and test cases.

First, open a new terminal in the project root path.

To run the project execute the following command:

```bash
make run
```

> NOTE: You can found the project's postman collection in the root folder.

Once the project is up and running you have to open an HTTP client like Postman and send the next request:

```sh
[POST] http://localhost:8001/messages/update
```

To reduce the test field of the exercise, performance and waiting time, there is an optional query param to choose if we want to obtain all the results in a paginated way or only the first 20 results. By default, the value of this query param is set to "false".

If you want to retrieve all search results:
```sh
[POST] http://localhost:8001/messages/update?showAll=true
```

Then in the project running terminal you'll be able to see a message with a URL requesting permissions to your Google Account for read-only level. Follow the URL and accept all permissions.

After accepting permissions, the program will create the corresponding tokens and will send a request through Gmail API retrieving all the messages matching the query.

Once the process completed, you should see a message **'Messages updated successfully!'** with status 200 in the HTTP client. This means that the messages were retrieved and persisted in the database successfully.

Finally, to see the persisted messages you can send the next request in the HTTP client to get the records from the database:

```sh
[GET] http://localhost:8001/messages
```

**To reset the user's credentials run the following command:**

```sh
make clean-tokens
```

## Database access

If you want to access into the internal H2 database you should have the program up and running and then on the web browser follow the next URL:

[http://localhost:8001/h2](http://localhost:8001/h2)

Credentials for database:

* Driver class: org.h2.Driver
* JDBC URL: jdbc:h2:mem:testdb
* User: sa
* Password: [empty]

## API Documentation

You can found the entire description of the differents available endpoints in the next URL. 

[http://localhost:8001/swagger-ui.html](http://localhost:8001/swagger.ui.html)

>NOTE: You should have the project up and running.
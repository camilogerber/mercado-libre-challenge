package com.mercadolibre.challenge.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmailHeader implements Serializable {

	private static final long serialVersionUID = 4606490065099676369L;

	@Id
	@Column(name = "id_message")
	private String id = UUID.randomUUID().toString();

	@Column(name = "message_date")
	private String date;

	@Column(name = "from_sender")
	private String fromSender;

	@Column(name = "subject")
	private String subject;

	@Override
	public String toString() {
		return "EmailHeader{" +
				"id='" + id + '\'' +
				", date='" + date + '\'' +
				", fromSender='" + fromSender + '\'' +
				", subject='" + subject + '\'' +
				'}';
	}
}

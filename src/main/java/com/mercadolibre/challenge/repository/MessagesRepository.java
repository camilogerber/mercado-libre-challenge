package com.mercadolibre.challenge.repository;

import com.mercadolibre.challenge.entities.EmailHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface MessagesRepository extends JpaRepository<EmailHeader, Serializable> {
}

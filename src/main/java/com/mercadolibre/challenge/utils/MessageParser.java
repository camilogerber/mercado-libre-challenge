package com.mercadolibre.challenge.utils;

import com.google.api.services.gmail.model.Message;
import com.mercadolibre.challenge.entities.EmailHeader;

public class MessageParser {

	public static EmailHeader parseMessageHeader(Message messageResult) {
		EmailHeader emailHeader = new EmailHeader();
		messageResult.getPayload().getHeaders().forEach(a -> {
			switch (a.getName()) {
				case "Subject":
					emailHeader.setSubject(a.getValue());
					break;
				case "From":
					emailHeader.setFromSender(a.getValue());
					break;
				case "Date":
					emailHeader.setDate(a.getValue());
					break;
			}
		});
		return emailHeader;
	}
}

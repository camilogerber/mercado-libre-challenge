package com.mercadolibre.challenge.controller;

import com.mercadolibre.challenge.entities.EmailHeader;
import com.mercadolibre.challenge.services.interfaces.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class EmailController {

	@Autowired
	private final EmailService emailService;

	public EmailController(EmailService emailService) {
		this.emailService = emailService;
	}

	@GetMapping("/messages")
	public ResponseEntity<List<EmailHeader>> getAllMessages() {
		List<EmailHeader> emailHeaders = emailService.getAllMessages();
		if (!emailHeaders.isEmpty()) {
			return ResponseEntity.ok().body(emailHeaders);
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping("/messages/update")
	public ResponseEntity<String> loadMessages(@RequestParam(defaultValue = "false") boolean showAll) {
		List<EmailHeader> result = emailService.updateMessages(showAll);
		if (!result.isEmpty()) {
			return ResponseEntity.ok().body("Messages updated successfully!");
		}
		return ResponseEntity.notFound().build();
	}
}

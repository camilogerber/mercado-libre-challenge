package com.mercadolibre.challenge.services.interfaces;

import com.mercadolibre.challenge.entities.EmailHeader;

import java.util.List;

public interface GmailAPI {
	List<EmailHeader> getAllUserEmails();

	List<EmailHeader> getUserEmails();
}

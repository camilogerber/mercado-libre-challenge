package com.mercadolibre.challenge.services.interfaces;

import com.mercadolibre.challenge.entities.EmailHeader;

import java.util.List;

public interface EmailService {
	List<EmailHeader> getAllMessages();

	List<EmailHeader> updateMessages(boolean showAll);
}

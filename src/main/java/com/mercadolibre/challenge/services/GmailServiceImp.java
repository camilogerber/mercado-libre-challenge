package com.mercadolibre.challenge.services;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.common.collect.ImmutableList;
import com.mercadolibre.challenge.entities.EmailHeader;
import com.mercadolibre.challenge.services.interfaces.GmailAPI;
import com.mercadolibre.challenge.utils.MessageParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class GmailServiceImp implements GmailAPI {

    public final String APPLICATION_NAME = "Mercado Libre Email Reader";
    public final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    public final String TOKENS_DIRECTORY_PATH = "tokens";
    public final String USER = "me";
    public final String QUERY = "{+\"DevOps\" AND NOT from: \"DevOps\"}";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private final List<String> SCOPES = Collections.singletonList(GmailScopes.GMAIL_READONLY);
    private final ImmutableList<String> METADATA_HEADERS = ImmutableList.of("Subject", "From", "Date");
    private final String CREDENTIALS_FILE_PATH = "/credentials.json";
    public final long MAX_RESULTS = 100L; //Max results per page
    public final long MAX_PARTIAL_RESULTS = 20L; //Max results in partial result

    @Override
    public List<EmailHeader> getAllUserEmails() {
        List<EmailHeader> result = new ArrayList<>();
        // Build a new authorized API client service.
        try {
            Gmail service = createConnection();
            // Print the messages in the user's account.
            ListMessagesResponse pagedResultSet = getMessageList(service, null, MAX_RESULTS);
            do {
                log.info("Result size: " + pagedResultSet.getMessages().size());
                for (Message message : pagedResultSet.getMessages()) {
                    Message messageResult = service.users()
                            .messages()
                            .get(USER, message.getId())
                            .setFormat("metadata")
                            .setMetadataHeaders(METADATA_HEADERS)
                            .execute();
                    result.add(MessageParser.parseMessageHeader(messageResult));
                }
                pagedResultSet = getMessageList(service, pagedResultSet.getNextPageToken(), MAX_RESULTS);
            } while (hasNext(pagedResultSet));
        } catch (IOException | GeneralSecurityException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    @Override
    public List<EmailHeader> getUserEmails() {
        List<EmailHeader> result = new ArrayList<>();
        try {
            Gmail service = createConnection();
            ListMessagesResponse pagedResultSet = getMessageList(service, null, MAX_PARTIAL_RESULTS);
            log.info("Result size: " + pagedResultSet.getMessages().size());
            for (Message message : pagedResultSet.getMessages()) {
                Message messageResult = service.users()
                        .messages()
                        .get(USER, message.getId())
                        .setFormat("metadata")
                        .setMetadataHeaders(METADATA_HEADERS)
                        .execute();
                result.add(MessageParser.parseMessageHeader(messageResult));
            }
        } catch (IOException | GeneralSecurityException e) {
            log.error(e.getMessage());
        }
        return result;
    }

    private Gmail createConnection() throws GeneralSecurityException, IOException {
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .setApplicationName(APPLICATION_NAME)
                .build();
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = GmailServiceImp.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
    }

    private ListMessagesResponse getMessageList(Gmail service, String nextPageToken, long maxResults) throws IOException {
        return service.users()
                .messages()
                .list(USER)
                .setQ(QUERY)
                .setMaxResults(maxResults)
                .setPageToken(nextPageToken)
                .execute();
    }


    private boolean hasNext(ListMessagesResponse response) {
        return response != null && response.getNextPageToken() != null && !response.getNextPageToken().isEmpty();
    }
}

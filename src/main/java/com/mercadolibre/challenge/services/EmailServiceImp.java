package com.mercadolibre.challenge.services;

import com.mercadolibre.challenge.entities.EmailHeader;
import com.mercadolibre.challenge.repository.MessagesRepository;
import com.mercadolibre.challenge.services.interfaces.EmailService;
import com.mercadolibre.challenge.services.interfaces.GmailAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailServiceImp implements EmailService {

    @Autowired
    private final MessagesRepository messagesRepository;

    @Autowired
    private final GmailAPI gmailAPI;

    public EmailServiceImp(MessagesRepository messagesRepository, GmailServiceImp gmailAPI) {
        this.messagesRepository = messagesRepository;
        this.gmailAPI = gmailAPI;
    }

    @Override
    public List<EmailHeader> getAllMessages() {
        return messagesRepository.findAll();
    }

    @Override
    public List<EmailHeader> updateMessages(boolean showAll) {
        List<EmailHeader> result;
        if (showAll) {
            result = gmailAPI.getAllUserEmails();
        } else {
            result = gmailAPI.getUserEmails();
        }

        if (!result.isEmpty()) {
            messagesRepository.deleteAll();
            messagesRepository.saveAll(result);
        }
        return result;
    }
}

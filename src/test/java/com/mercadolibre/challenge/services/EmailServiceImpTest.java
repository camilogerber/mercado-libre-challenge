package com.mercadolibre.challenge.services;

import com.mercadolibre.challenge.entities.EmailHeader;
import com.mercadolibre.challenge.repository.MessagesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EmailServiceImpTest {

    @InjectMocks
    private EmailServiceImp emailService;

    @Mock
    private MessagesRepository repository;

    @Mock
    private GmailServiceImp gmailAPI;

    @Mock
    private EmailHeader emailHeader;

    private static final boolean SHOW_ALL = true;

    @Test
    public void shouldReturnAllMessages(){
        List<EmailHeader> expected = new ArrayList<>();
        EmailHeader email1 = new EmailHeader(
                "a7f0cb39-e897-443f-90d2-f12a6f3b1722",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Camilo Gerber <camilogerber1@gmail.com>",
                "DevOps Testing challenge mercado libre 2"
        );

        EmailHeader email2 = new EmailHeader(
                "ba16e64b-1a61-4747-ba24-1bad3ae2a08a",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "LinkedIn <jobs-listings@linkedin.com>",
                "Data Engineer / Senior Data Engineer and 107 more jobs for you, Camilo"
        );

        EmailHeader email3 = new EmailHeader(
                "426754d8-e515-4e3d-9141-f155cbdad088",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Get on Board <team@getonbrd.com>",
                "Desarrollador Full-Stack and 106 more jobs for you, Camilo"
        );
        expected.add(email1);
        expected.add(email2);
        expected.add(email3);
        when(repository.findAll()).thenReturn(expected);

        List<EmailHeader> result = emailService.getAllMessages();

        assertFalse(result.isEmpty());
        assertEquals(expected,result);
    }

    @Test
    public void shouldReturnEmptyListWhenEmailsNotFound(){
        when(gmailAPI.getAllUserEmails()).thenReturn(new ArrayList<>());

        List<EmailHeader> result = emailService.updateMessages(SHOW_ALL);

        assertTrue(result.isEmpty());
    }

    @Test
    public void shouldReturnNonEmptyListWhenEmailsFound(){
        List<EmailHeader> expected = new ArrayList<>();
        EmailHeader email1 = new EmailHeader(
                "a7f0cb39-e897-443f-90d2-f12a6f3b1722",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Camilo Gerber <camilogerber1@gmail.com>",
                "DevOps Testing challenge mercado libre 2"
        );

        EmailHeader email2 = new EmailHeader(
                "ba16e64b-1a61-4747-ba24-1bad3ae2a08a",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "LinkedIn <jobs-listings@linkedin.com>",
                "Data Engineer / Senior Data Engineer and 107 more jobs for you, Camilo"
        );

        EmailHeader email3 = new EmailHeader(
                "426754d8-e515-4e3d-9141-f155cbdad088",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Get on Board <team@getonbrd.com>",
                "Desarrollador Full-Stack and 106 more jobs for you, Camilo"
        );
        expected.add(email1);
        expected.add(email2);
        expected.add(email3);

        when(gmailAPI.getAllUserEmails()).thenReturn(expected);

        List<EmailHeader> result = emailService.updateMessages(SHOW_ALL);

        assertFalse(result.isEmpty());
    }


}
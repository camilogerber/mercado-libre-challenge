package com.mercadolibre.challenge.controller;

import com.mercadolibre.challenge.entities.EmailHeader;
import com.mercadolibre.challenge.services.EmailServiceImp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = EmailController.class)
class EmailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmailServiceImp emailService;

    private static final boolean SHOW_ALL = true;

    private final List<EmailHeader> emailList = new ArrayList<>();

    @BeforeEach
    void setUp() {
        EmailHeader email1 = new EmailHeader(
                "a7f0cb39-e897-443f-90d2-f12a6f3b1722",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Camilo Gerber <camilogerber1@gmail.com>",
                "DevOps Testing challenge mercado libre 2"
        );

        EmailHeader email2 = new EmailHeader(
                "ba16e64b-1a61-4747-ba24-1bad3ae2a08a",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "LinkedIn <jobs-listings@linkedin.com>",
                "Data Engineer / Senior Data Engineer and 107 more jobs for you, Camilo"
        );

        EmailHeader email3 = new EmailHeader(
                "426754d8-e515-4e3d-9141-f155cbdad088",
                "Sat, 26 Sep 2020 15:37:48 -0300",
                "Get on Board <team@getonbrd.com>",
                "Desarrollador Full-Stack and 106 more jobs for you, Camilo"
        );
        emailList.add(email1);
        emailList.add(email2);
        emailList.add(email3);
    }

    @Test
    void shouldFetchAllEmails() throws Exception {
        when(emailService.getAllMessages()).thenReturn(emailList);

        this.mockMvc.perform(get("/messages"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.size()").value(emailList.size()));
    }

    @Test
    void shouldReturnNotFoundStateWhenNoEmailsFound() throws Exception {
        List<EmailHeader> emailHeaders = Collections.emptyList();

        when(emailService.getAllMessages()).thenReturn(emailHeaders);

        this.mockMvc.perform(get("/messages"))
                .andExpect(status().isNotFound());
    }

    @Test
    void shouldReturn200WhenUsersUpdatedSuccessfully() throws Exception {
        when(emailService.updateMessages(false)).thenReturn(emailList);

        this.mockMvc.perform(post("/messages/update"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturn200WhenUsersUpdatedSuccessfullyWithFlag() throws Exception {
        when(emailService.updateMessages(true)).thenReturn(emailList);

        this.mockMvc.perform(post("/messages/update?showAll=true"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldReturn404WhenUsersResponseIsEmpty() throws Exception {
        when(emailService.updateMessages(SHOW_ALL)).thenReturn(Collections.emptyList());

        this.mockMvc.perform(post("/messages/update"))
                .andExpect(status().isNotFound());
    }


}